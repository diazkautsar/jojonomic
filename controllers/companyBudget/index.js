const _ = require('lodash')
const { companyBudget, company, transaction } = require('../../models')

module.exports = {
  insertBudget: async (req, res, next) => {
    try {
      const findCompany = await company.findCompany({ name: req.body.company_name })
      if (_.isEmpty(findCompany)) {
        return next({
          msg: "Company not Found",
          status: 404,
        })
      }
      
      const company_id = findCompany[0].id
      const getCompanyBudget = await companyBudget.getCompanyBudget({
        company_id,
        ...req.body
      })

      if (_.isEmpty(getCompanyBudget)) {
        // create new budget
        const result = await companyBudget.insertBudget({
          company_id,
          ...req.body
        })
        return result && res.status(201).json({
          msg: "Success insert budget for this company",
          data: result
        })        
      } else {
        // update bugdet
        const result = await companyBudget.addBudget({
          company_id,
          ...req.body
        })
        return result && res.status(201).json({
          msg: "Success update budget",
          data: result
        })
      }

    } catch (error) {
      console.log(error)
      next()
    }
  },
  getCompanyBudget: async (req, res, next) => {
    try {
      const { id } = req.params
      const result = await companyBudget.getCompanyBudget({
        company_id: id,
        ...req.body
      })
      if (_.isEmpty(result)) {
        return next({
          msg: "Company Budget not Found",
          data: []
        })
      } else {
        return res.status(200).json({
          data: result
        })
      }
    } catch (error) {
      console.log(error)
      next()
    }
  },
  getListBudgetCompany: async (req, res, next) => {
    try {
      const result = await companyBudget.getListCompanyBudget()
      if (_.isEmpty(result)) {
        return next({
          msg: "Company Budget not Found",
          data: []
        })
      }
      return res.status(200).json({
        data: result
      })
    } catch (error) {
      console.log(error)
      next()
    }
  }
}