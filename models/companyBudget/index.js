const db = require('../../config/database')

module.exports = {
  insertBudget: (param) => {
    return new Promise ((resolve, reject) => {
      const { amount, company_id } = param
      const query = `
      INSERT INTO tb_company_budget (amount, company_id)
      VALUES ('${Number(amount)}', '${Number(company_id)}')
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
  getCompanyBudget: (param) => {
    return new Promise ((resolve, reject) => {
      const { id, company_id } = param
      const query = `
      SELECT 
        cb.amount,
        c.name as company_name
      FROM tb_company_budget cb
      INNER JOIN tb_company c ON c.id = '${company_id}'
      WHERE cb.id = '${Number(id)}' or cb.company_id = '${Number(company_id)}'
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
  addBudget: (param) => {
    return new Promise ((resolve, reject) => {
      const { amount, company_id } = param
      const query = `
      UPDATE tb_company_budget cb
      SET
        cb.amount = (cb.amount + '${amount}')
      WHERE
        cb.company_id = '${company_id}'
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
  getListCompanyBudget: (param) => {
    return new Promise ((resolve, reject) => {
      const query = `
      SELECT
        c.name as company_name,
        c.address,
        cb.amount
      FROM tb_company_budget cb
      INNER JOIN tb_company c ON c.id = cb.company_id
      `
      db.query(query, (err, result) => {
				if (err) {
					return reject(err)
				}
				return resolve(result)
			})
    })
  },
  subtractBudget: (param) => {
    return new Promise ((resolve, reject) => {
      const { amount, company_id } = param
      const query = `
      UPDATE tb_company_budget cb
      SET
        cb.amount = (cb.amount - '${amount}')
      WHERE
        cb.company_id = '${company_id}'
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
}