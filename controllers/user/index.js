const _ = require('lodash')
const { user, company } = require('../../models')

module.exports = {
  registerUser: async (req, res, next) => {
    try {
      const findCompany = await company.findCompany({ name: req.body.company_name })
      if (_.isEmpty(findCompany)) {
        return next({
          msg: 'Company name not Found',
          status: 404,
        })
      } 
      const isExistUser = await user.findUser(req.body)
      if (!_.isEmpty(isExistUser)) {
        return next({
          msg: "Email already used",
          status: 401,
        })
      }
      
      const company_id = findCompany[0].id
      const result = await user.createUser({
        company_id,
        ...req.body
      })
      return result && res.status(201).json({
        msg: "Register user success",
        data: result
      })
    } catch (err) {
      console.log(err)
      next()
    }
  },
  getUser: async (req, res, next) => {
    try {
      const { id } = req.params
      const result = await user.findUser({ id })
      if (_.isEmpty(result)) {
        return next({
          msg: "User not found",
          data: []
        })
      } else {
        return res.status(200).json({
          data: result
        })
      }
    } catch (err) {
      console.log(err)
      next()
    }
  },
  getListUser: async (req, res, next) => {
    try {
      const result = await user.listUser()
      if (_.isEmpty(result)) {
        return next({
          msg: "Data not found",
          data: []
        })
      }

      return res.status(200).json(result)
    } catch (error) {
      console.log(error)
      next()
    }
  },
  deleteUser: async (req, res, next) => {
    try {
      const result = await user.deleteUser(req.body)
      return result && res.status(200).json({
        msg: "Delete Success"
      })
    } catch (error) {
      console.log(error)
      next()
    }
  }
}