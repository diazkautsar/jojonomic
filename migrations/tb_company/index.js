require('dotenv').config()

const mysql = require('mysql')
const connection = mysql.createConnection({
  connectTimeout: 10,
  host: 'localhost',
  user: process.env.USER_DB,
  password: process.env.PASSWORD_DB,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
})

const queryDelete = `
DROP TABLE IF EXISTS tb_company
`

const queryCreate = `
CREATE TABLE IF NOT EXISTS tb_company(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR (255) NOT NULL,
address VARCHAR (255) NOT NULL,
created_at TIMESTAMP NOT NULL DEFAULT NOW(),
updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now(),
PRIMARY KEY (id))`;

connection.query(queryDelete, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success drop tb_company')
})

connection.query(queryCreate, (err, resuls) => {
  if (err) console.log(err)
  else 
    console.log('success create tb_company')
})

connection.end()