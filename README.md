## jojonomic

To use these app:

1. npm install
2. touch .env
3. copy value form env-example to .env. insert data to .env
4. npm run migrations
5. npm run seeders
6. npm run start