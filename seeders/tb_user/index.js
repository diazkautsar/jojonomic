require('dotenv').config()

const mysql = require('mysql')
const connection = mysql.createConnection({
  connectTimeout: 10,
  host: 'localhost',
  user: process.env.USER_DB,
  password: process.env.PASSWORD_DB,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
})

const queryInsert = `
INSERT INTO tb_user (
  first_name,
  last_name,
  email,
  amount,
  bank_name,
  bank_number,
  company_id
)
VALUES ('Djalal', 'Kurnia', 'djalal@mail.com', '${10000}', 'BNI', '${890817}', '${1}')
`
const queryInsert1 = `
INSERT INTO tb_user (
  first_name,
  last_name,
  email,
  amount,
  bank_name,
  bank_number,
  company_id
)
VALUES ('Dimas', 'Kusen', 'dimas@mail.com', '${10000}', 'BJB', '${8908897}', '${1}')
`

const queryInsert2 = `
INSERT INTO tb_user (
  first_name,
  last_name,
  email,
  amount,
  bank_name,
  bank_number,
  company_id
)
VALUES ('Dudung', 'Konelo', 'dudung@mail.com', '${10000}', 'BRI', '${78134987}', '${2}')
`

connection.query(queryInsert, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success insert to tb_user')
})

connection.query(queryInsert1, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success insert to tb_user')
})

connection.query(queryInsert2, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success insert to tb_user')
})

connection.end()