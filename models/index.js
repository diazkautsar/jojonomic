const user = require('./user')
const company = require('./company')
const companyBudget = require('./companyBudget')
const transaction = require('./transaction')

module.exports = {
  company,
  user,
  companyBudget,
  transaction,
}