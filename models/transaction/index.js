const db = require('../../config/database')

module.exports = {
  insertTransaction: (param) => {
    return new Promise ((resolve, reject) => {
      const { type, amount, company_id, user_id } = param
      const query = `
      INSERT INTO tb_transaction (type, amount, company_id, user_id)
      VALUES ('${type}', '${Number(amount)}', '${Number(company_id)}', '${Number(user_id)}')
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
  checkUserCompany: (param) => {
    return new Promise ((resolve, reject) => {
      const { company_name, user_email } = param
      const query = `
      SELECT 
        u.first_name,
        u.last_name,
        u.email,
        u.bank_name,
        u.bank_number,
        c.name as company_name,
        c.id as company_id,
        u.id as user_id
      FROM
        tb_user u
      INNER JOIN 
        tb_company c ON c.id = u.company_id
      WHERE
        c.name = '${company_name}' AND u.email = '${user_email}';
      `
      db.query(query, (err, result) => {
				if (err) {
					return reject(err)
				}
				return resolve(result)
			})
    })
  },
  logTransaction: (param) => {
    return new Promise ((resolve, reject) => {
      const query = `
      SELECT 
        u.email as email,
        u.bank_number as account_number,
        c.name as company_name,
        t.type as transaction_type,
        cb.amount as amount,
        t.created_at as transaction_date
      FROM tb_transaction t
      INNER JOIN tb_company c ON c.id = t.company_id
      INNER JOIN tb_user u ON u.id = t.user_id
      INNER JOIN tb_company_budget cb ON cb.id = t.company_id;
      `
      db.query(query, (err, result) => {
				if (err) {
					return reject(err)
				}
				return resolve(result)
			})
    })
  }
}