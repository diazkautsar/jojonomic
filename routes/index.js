const router = require('express').Router()

const { company, user, companyBudget, transaction } = require('../controllers')

router.post('/createUser', user.registerUser)
router.get('/getUser/:id', user.getUser)
router.get('/getlistUser', user.getListUser)
router.delete('/deleteUser', user.deleteUser)

router.post('/createCompany', company.registerCompany)
router.get('/getCompany/:id', company.getCompany)
router.get('/getListCompany', company.getListCompany)
router.delete('/deleteCompany', company.deleteCompany)

router.post('/createBudget', companyBudget.insertBudget)
router.get('/getBudgetCompany/:id', companyBudget.getCompanyBudget)
router.get('/getListBudgetCompany', companyBudget.getListBudgetCompany)

router.post('/reimburse', transaction.reimburse)
router.post('/disburse', transaction.disburse)
router.post('/close', transaction.close)
router.get('/getLogTransaction', transaction.logTransaction)

module.exports = router