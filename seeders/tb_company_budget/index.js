require('dotenv').config()

const mysql = require('mysql')
const connection = mysql.createConnection({
  connectTimeout: 10,
  host: 'localhost',
  user: process.env.USER_DB,
  password: process.env.PASSWORD_DB,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
})

const queryInsert = `
INSERT INTO tb_company_budget (amount, company_id)
VALUES ('${10000000}', '${1}')
`
const queryInsert1 = `
INSERT INTO tb_company_budget (amount, company_id)
VALUES ('${20000000}', '${2}')
`

connection.query(queryInsert, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success insert to tb_company_budget')
})

connection.query(queryInsert1, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success insert to tb_company_budget')
})

connection.end()