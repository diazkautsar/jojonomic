const _ = require('lodash')
const { transaction, user, company, companyBudget } = require('../../models')

module.exports = {
  reimburse: async (req, res, next) => {
    try {
      const { type, amount, user_email } = req.body
      // check user and company valid or not
      const result = await transaction.checkUserCompany(req.body)
      if (_.isEmpty(result)) {
        return next({
          msg: "User or Company name invalid",
          status: 404,
        })
      }

      const company_id = result[0].company_id
      const user_id = result[0].user_id

      const insertReimburse = await transaction.insertTransaction({
        type: type && type === "R" ? type : "R",
        amount,
        company_id,
        user_id,
      })

      if (insertReimburse) {
        const updateBudget = await companyBudget.subtractBudget({
          amount,
          company_id,
        })
        if (updateBudget) {
          return next({
            msg: `Reimburese to ${user_email} success`,
            status: 201,
          })
        } else {
          return next({
            msg: "Reimburse failed",
            status: 400
          })
        }
      } else {
        return next({
          msg: "Reimburse failed",
          status: 400
        })
      }

    } catch (error) {
      console.log(error)
      next()
    }
  },
  disburse: async (req, res, next) => {
    try {
      const { type, amount, user_email } = req.body
      // check user and company valid or not
      const result = await transaction.checkUserCompany(req.body)
      if (_.isEmpty(result)) {
        return next({
          msg: "User or Company name invalid",
          status: 404,
        })
      }

      const company_id = result[0].company_id
      const user_id = result[0].user_id

      const insertReimburse = await transaction.insertTransaction({
        type: type && type === "C" ? type : "C",
        amount,
        company_id,
        user_id,
      })

      if (insertReimburse) {
        const updateBudget = await companyBudget.subtractBudget({
          amount,
          company_id,
        })
        if (updateBudget) {
          return next({
            msg: `Disburse to ${user_email} success`,
            status: 201,
          })
        } else {
          return next({
            msg: "Disburse failed",
            status: 400
          })
        }
      } else {
        return next({
          msg: "Disburse failed",
          status: 400
        })
      }

    } catch (error) {
      console.log(error)
      next()
    }
  },
  close: async (req, res, next) => {
    try {
      const { type, amount, user_email } = req.body
      // check user and company valid or not
      const result = await transaction.checkUserCompany(req.body)
      if (_.isEmpty(result)) {
        return next({
          msg: "User or Company name invalid",
          status: 404,
        })
      }

      const company_id = result[0].company_id
      const user_id = result[0].user_id

      const insertReimburse = await transaction.insertTransaction({
        type: type && type === "C" ? type : "C",
        amount,
        company_id,
        user_id,
      })

      if (insertReimburse) {
        const updateBudget = await companyBudget.addBudget({
          amount,
          company_id,
        })
        if (updateBudget) {
          return next({
            msg: `close success`,
            status: 201,
          })
        } else {
          return next({
            msg: "close failed",
            status: 400
          })
        }
      } else {
        return next({
          msg: "close failed",
          status: 400
        })
      }

    } catch (error) {
      console.log(error)
      next()
    }
  },
  logTransaction: async (req, res, next) => {
    try {
      const result = await transaction.logTransaction()
      return result && res.status(200).json({
        msg: result
      })
    } catch (error) {
      console.log(error)
      next()
    }
  }
}