require('dotenv').config()

const mysql = require('mysql')
const connection = mysql.createConnection({
  connectTimeout: 10,
  host: 'localhost',
  user: process.env.USER_DB,
  password: process.env.PASSWORD_DB,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
})

const queryDelete = `
DROP TABLE IF EXISTS tb_company_budget
`

const queryCreate = `
CREATE TABLE IF NOT EXISTS tb_company_budget(
  id INT NOT NULL AUTO_INCREMENT,
  amount INT NOT NULL,
  company_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (company_id) REFERENCES tb_company(id) ON DELETE CASCADE ON UPDATE CASCADE)`;

connection.query(queryDelete, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success drop tb_company_budget')
})

connection.query(queryCreate, (err, resuls) => {
  if (err) console.log(err)
  else 
    console.log('success create tb_company_budget')
})

connection.end()