const _ = require('lodash')
const { company } = require('../../models')

module.exports = {
  registerCompany: async (req, res, next) => {
    try {
      const isExist = await company.findCompany(req.body)
      if (!_.isEmpty(isExist)) {
        return next({
          msg: 'Name already exist',
          status: 401,
        })
      } else {
        const result = await company.registerCompany(req.body)
        return result && res.status(201).json({
          msg: 'Register Success',
          data: result,
        })
      }
    } catch (err) {
      return next()
    }
  },
  getCompany: async (req, res, next) => {
    try {
      const { id } = req.params
      const result = await company.findCompany({ id })
      if (_.isEmpty(result)) {
        return next({
          msg: "Company not found",
          data: [],
        })
      }
      return res.status(200).json({
        data: result
      })
    } catch (error) {
      console.log(error)
      next()
    }
  },
  getListCompany: async (req, res, next) => {
    try {
      const result = await company.listCompany()
      if (_.isEmpty(result)) {
        return next({
          msg: "Data not found",
          data: [],
        })
      }
      return res.status(200).json(result)
    } catch (error) {
      console.log(error)
      next()
    }
  },
  deleteCompany: async (req, res, next) => {
    try {
      const result = await company.deleteCompany(req.body)
      return result && res.status(200).json({
        msg: "Delete Success"
      })
    } catch (error) {
      console.log(error)
      next()
    }
  }
}