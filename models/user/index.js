const db = require('../../config/database')

module.exports = {
	findUser: (param) => {
		return new Promise ((resolve, reject) => {
      const { email, id, } = param
			const query = `
      SELECT
        u.id,
        u.first_name,
        u.last_name,
        u.email,
        u.bank_name,
        u.bank_number,
        c.name as company_name
      FROM
        tb_user u
      INNER JOIN
        tb_company c ON c.id = u.company_id
			WHERE u.email = '${email}' or u.id = '${Number(id)}'
			`
			db.query(query, (err, result) => {
				if (err) {
					return reject(err)
				}
				return resolve(result)
			})
		})
	},
	createUser: (param) => {
		return new Promise ((resolve, reject) => {
      const { first_name, last_name, email, bank_name, bank_number, amount, company_id } = param
      const query = `
      INSERT INTO tb_user (first_name, last_name, email, bank_name, bank_number, amount, company_id)
      VALUES ('${first_name}', '${last_name}', '${email}', '${bank_name}', '${Number(bank_number)}', '${Number(amount)}', '${company_id}')
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
		})
  },
  listUser: (param) => {
    return new Promise ((resolve, reject) => {
      const query = `
      SELECT
        u.first_name,
        u.last_name,
        u.email,
        u.bank_name,
        u.bank_number,
        c.name as company_name
      FROM
        tb_user u
      INNER JOIN
        tb_company c ON c.id = u.company_id
      `
      db.query(query, (err, result) => {
				if (err) {
					return reject(err)
				}
				return resolve(result)
			})
    })
  },
  deleteUser: (param) => {
    return new Promise ((resolve, reject) => {
      const { id, email } = param
      const query = `
      DELETE FROM tb_user
      WHERE email = '${email}'
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  }
}
