const company = require('./company')
const user = require('./user')
const companyBudget = require('./companyBudget')
const transaction = require('./transaction')

module.exports = {
  company,
  user,
  companyBudget,
  transaction,
}