require('dotenv').config()

const mysql = require('mysql')
const connection = mysql.createConnection({
  connectTimeout: 10,
  host: 'localhost',
  user: process.env.USER_DB,
  password: process.env.PASSWORD_DB,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
})

const queryInsert = `
INSERT INTO tb_company (name, address)
VALUES ('PT Makmur Sejahtera Selamanya', 'Bandung')
`
const queryInsert1 = `
INSERT INTO tb_company (name, address)
VALUES ('PT Untung Terus Semoga Tidak Rugi', 'Jakarta')
`

connection.query(queryInsert, queryInsert1, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success insert to tb_company')
})

connection.query(queryInsert1, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success insert to tb_company')
})

connection.end()