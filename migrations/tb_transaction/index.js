require('dotenv').config()

const mysql = require('mysql')
const connection = mysql.createConnection({
  connectTimeout: 10,
  host: 'localhost',
  user: process.env.USER_DB,
  password: process.env.PASSWORD_DB,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
})

const queryDelete = `
DROP TABLE IF EXISTS tb_transaction
`

const queryCreate = `
CREATE TABLE IF NOT EXISTS tb_transaction(
id INT NOT NULL AUTO_INCREMENT,
type VARCHAR (255) NOT NULL,
amount INT NOT NULL,
company_id INT NOT NULL,
user_id INT NOT NULL,
created_at TIMESTAMP NOT NULL DEFAULT NOW(),
updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now(),
PRIMARY KEY (id),
FOREIGN KEY (user_id) REFERENCES tb_user(id) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (company_id) REFERENCES tb_company(id) ON DELETE CASCADE ON UPDATE CASCADE)`;

connection.query(queryDelete, (err, result) => {
  if (err) console.log(err)
  else 
    console.log('success drop tb_transaction')
})

connection.query(queryCreate, (err, resuls) => {
  if (err) console.log(err)
  else 
    console.log('success create tb_transaction')
})

connection.end()