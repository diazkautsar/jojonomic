const db = require('../../config/database')

module.exports = {
  findCompany: (param) => {
    return new Promise ((resolve, reject) => {
      const { name, id } = param
      const query = `
      SELECT * FROM tb_company
      WHERE name = '${name}' or id = '${Number(id)}'
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
  registerCompany: (param) => {
    return new Promise ((resolve, reject) => {
      const { name, address } = param
      const query = `
      INSERT INTO tb_company (name, address)
      VALUES ('${name}', '${address}')
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
  listCompany: (param) => {
    return new Promise ((resolve, reject) => {
      const query = `
      SELECT
        c.name,
        c.address
      FROM
        tb_company c
      `
      db.query(query, (err, result) => {
				if (err) {
					return reject(err)
				}
				return resolve(result)
			})
    })
  },
  deleteCompany: (param) => {
    return new Promise ((resolve, reject) => {
      const { id, name } = param
      const query = `
      DELETE FROM tb_company
      WHERE id = '${Number(id)}' OR name = '${name}'
      `
      db.query(query, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    })
  },
}